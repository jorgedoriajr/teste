package br.com.doria;

import java.util.Scanner;

public class Aplicativo {

	public static void main(String[] args) {
		
		//declara��o de vari�veis
		double av1, av2, media;
		Scanner tecla = new Scanner(System.in);
		
		//Entrada de dados
		System.out.println("Digite AV1:");
		av1 = tecla.nextDouble();
		System.out.println("Digite AV2:");
		av2 = tecla.nextDouble();
		
		//Processamento de dados
		media = (av1+av2)/2;
		
		//Sa�da da informa��o
		if (media > 7){
			System.out.println("Aprovado! \n sua m�dia e: " + media);
		}else{
			System.out.println("Reprovado!");
			
		}
				
	}
	
}
